/**
 * @brief Program membaca sensor ultrasonic
 * 
 */

/*
Buat Program dimana kondisi yang diharapkan : LED
HIJAU akan menyala jika jaraknya lebih dari 100 cm
dan LED MERAH akan menyala jika jaraknya kurang
dari 100 cm dari subjek
*/

// inisialisasi pin
const int TRIG_PIN = 12;
const int ECHO_PIN = 11;
const int LED_HIJAU = 13;
const int LED_MERAH = 10;

void setup()
{
    Serial.begin(9600);
    pinMode(TRIG_PIN, OUTPUT);  // set pin TRIG sebagai output
    pinMode(ECHO_PIN, INPUT);   // set pin ECHO sebagai input
    pinMode(LED_HIJAU, OUTPUT); // set pin LED_HIJAU sebagai output
    pinMode(LED_MERAH, OUTPUT); // set pin LED_MERAH sebagai output
}

void loop()
{
    long duration;
    float distanceCm;

    digitalWrite(TRIG_PIN, LOW); // set TRIG_PIN ke LOW terlebih dahulu
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN, HIGH); // set TRIG_PIN ke HIGH untuk mengirim sinyal ultrasonic
    delayMicroseconds(10);
    digitalWrite(TRIG_PIN, LOW); // set TRIG_PIN ke LOW setelah mengirim sinyal ultrasonic

    duration = pulseIn(ECHO_PIN, HIGH); // baca durasi waktu dari sinyal yang dikirimkan ke ECHO_PIN
    distanceCm = duration * 0.034 / 2;  // hitung jarak berdasarkan durasi waktu yang didapat

    Serial.print(distanceCm); // tampilkan jarak pada serial monitor
    Serial.println(" cm");

    if (distanceCm > 100)
    {                                  // jika jarak lebih dari atau sama dengan 100 cm
        digitalWrite(LED_HIJAU, HIGH); // nyalakan LED_HIJAU
        digitalWrite(LED_MERAH, LOW);  // matikan LED_MERAH
    }
    else
    {                                  // jika jarak kurang dari 100 cm
        digitalWrite(LED_HIJAU, LOW);  // matikan LED_HIJAU
        digitalWrite(LED_MERAH, HIGH); // nyalakan LED_MERAH
    }

    delay(100); // delay selama 100 milidetik
}