/**
 * @brief Program membaca sensor pendeteksi cahaya LDR
 * 
 */

/*
  Buat Rangkaian dengan LED 2 (Warna Merah
  dan Biru) dan Photoresistor
  1. Jika nilai Photoresistor <250 Kedua LED
  masih Mati
  2. Jika Nilai Photoresistor >249 dan <500
  maka LED MERAH HIDUP
  3. Jika Nilai Photoresistor >499 maka LED
  BIRU HIDUP
*/

#define LED_MERAH 6 // deklarasi pin LED merah sebagai konstanta dengan nilai 6
#define LED_BIRU 3  // deklarasi pin LED biru sebagai konstanta dengan nilai 3

int adcValue; // variabel untuk menyimpan nilai ADC dari sensor LDR

void setup()
{
    pinMode(LED_MERAH, OUTPUT); // konfigurasi pin LED merah sebagai output
    pinMode(LED_BIRU, OUTPUT);  // konfigurasi pin LED biru sebagai output

    Serial.begin(9600); // inisialisasi koneksi serial monitor dengan baud rate 9600
}

void loop()
{
    adcValue = analogRead(A0);                        // baca nilai ADC dari sensor LDR
    Serial.println("Nilai ADC: " + String(adcValue)); // tampilkan nilai ADC pada serial monitor

    if (adcValue < 250)
    {                                 // jika nilai ADC kurang dari 250
        digitalWrite(LED_MERAH, LOW); // matikan LED merah pada pin 6
        digitalWrite(LED_BIRU, LOW);  // matikan LED biru pada pin 3
    }
    else if (adcValue >= 250 && adcValue < 500)
    {                                  // jika nilai ADC antara 250 dan 499
        digitalWrite(LED_MERAH, HIGH); // nyalakan LED merah pada pin 6
    }
    else
    {                                 // jika nilai ADC lebih besar atau sama dengan 500
        digitalWrite(LED_BIRU, HIGH); // nyalakan LED biru pada pin 3
    }

    delay(100); // jeda selama 100 milidetik
}
