// Mendefinisikan Pin LED dengan menggunakan define
/**
 * @brief Program Membaca sensor suhu
 * 
 */

/*
  Buat Rangkaian dengan LED dan Temp Sensor
  1. Jika nilai Temp dibawah 30C maka LED
  Hijau Hidup
  2. Jika Nilai Temp diatas 35C maka LED
  Merah Hidup
  3. Jika Nilai Temp antara 30 dan 35 maka
  LED Biru Hidup  
*/

#define LED_HIJAU 2
#define LED_BIRU 3
#define LED_MERAH 4

// Mendefinisikan variabel
int baselineTemp = 0;
int celsius = 0;

void setup()
{
    // Mengatur Pin LED sebagai Output
    pinMode(LED_HIJAU, OUTPUT);
    pinMode(LED_BIRU, OUTPUT);
    pinMode(LED_MERAH, OUTPUT);
    // Mengatur Pin Sensor sebagai Input
    pinMode(A0, INPUT);
    // Mengatur komunikasi Serial
    Serial.begin(9600);
}

void loop()
{
    // Set nilai baseline temperature
    baselineTemp = 30;

    // Mengambil nilai suhu dari sensor dan mengkonversi ke derajat Celsius
    celsius = map(((analogRead(A0) - 20) * 3.04), 0, 1023, -40, 125);

    // Mencetak nilai suhu ke Serial Monitor
    Serial.print("Suhu: ");
    Serial.print(celsius);
    Serial.println(" C");

    // Menyalakan LED hijau jika suhu di bawah baseline
    if (celsius < baselineTemp)
    {
        digitalWrite(LED_HIJAU, HIGH);
        digitalWrite(LED_BIRU, LOW);
        digitalWrite(LED_MERAH, LOW);
    }
    // Menyalakan LED biru jika suhu di antara baseline dan 35 derajat Celsius
    if (celsius >= baselineTemp && celsius <= 35)
    {
        digitalWrite(LED_HIJAU, LOW);
        digitalWrite(LED_BIRU, HIGH);
        digitalWrite(LED_MERAH, LOW);
    }
    // Menyalakan LED merah jika suhu di atas 35 derajat Celsius
    if (celsius > 35)
    {
        digitalWrite(LED_HIJAU, LOW);
        digitalWrite(LED_BIRU, LOW);
        digitalWrite(LED_MERAH, HIGH);
    }

    // Delay selama 1 detik
    delay(1000);
}