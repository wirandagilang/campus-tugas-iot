# campus-tugas-iot

- [campus-tugas-iot](#campus-tugas-iot)
  - [Tugas 1](#tugas-1)
  - [Tugas 2](#tugas-2)
  - [Tugas 3](#tugas-3)

## Youtube

> Tugas 1
https://youtu.be/cjQqh-e6Ef4

>Tugas 2
https://youtu.be/L-7mEngpJrM

>Tugas 3
https://youtu.be/tj4orP8iPHY


## Tugas 1
![alt text](/doc/tugas-1.png)

>Membaca sensor cahaya dengan ketentuan berikut:
>1. Jika nilai Photoresistor <250→ Kedua LED masih Mati
>2. Jika Nilai Photoresistor >249 dan <500 maka LED MERAH HIDUP
>3. Jika Nilai Photoresistor >499 maka LED BIRU HIDUP

## Tugas 2
![alt text](/doc/tugas-2.png)

>Membuat sebuah program senderhana menggunakan ultrasonic sensor, dengan ketentuan berikut:
Jika nilai PhotoresistorBuat Program dimana kondisi yang diharapkan : LED
HIJAU akan menyala jika jaraknya lebih dari 100 cm
dan LED MERAH akan menyala jika jaraknya kurang
dari 100 cm dari subjek

## Tugas 3
![alt text](/doc/tugas-3.png)

> Membaca sensor suhu dengan ketentuan sebagai berikut
>1. Jika nilai Temp dibawah 30C maka LED Hijau Hidup
>2. Jika Nilai Temp diatas 35C maka LED Merah Hidup
>3. Jika Nilai Temp antara 30 dan 35 maka Kedua LED Biru Hidup